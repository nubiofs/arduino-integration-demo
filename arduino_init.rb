require './arduino_actuator_sensor.rb'

class ArduinoInit

  attr_accessor :thr
  attr_accessor :ar

  def start_arduino
    @thr = Thread.new do
      @ar = ArduinoActuatorSensor.new
    end
  end
end