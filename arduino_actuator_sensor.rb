require "rubygems"
require "arduino_firmata"
require "rest-client"
require "json"

class ArduinoActuatorSensor

  RED_PIN = 2
  YELLOW_PIN = 3
  GREEN_PIN = 4

  ARDUINO_PUT_URL = 'http://localhost:3000/basic_resources/1/components/100000/actuate/arduino_luminosity'
  ARDUINO_GET_URL = 'http://localhost:3000/basic_resources/1/components/100000/collect/arduino_traffic_light'

  @arduino = ArduinoFirmata.connect
  puts "firmata version #{@arduino.version}"

  attr_accessor :luminosity_sensor

  def initialize
    @set_traffic_light = 'red'
  end

  def self.update_arduino_adaptor
    arduino_traffic_light_json = JSON.parse(RestClient.get ARDUINO_GET_URL)
    @set_traffic_light = arduino_traffic_light_json['data']

    RestClient.put ARDUINO_PUT_URL, {'data'=>{'value' => @luminosity_sensor} }, :content_type => :json, :accept => :json
  end

  loop do
    self.update_arduino_adaptor

    @luminosity_sensor = @arduino.analog_read 0
    puts @luminosity_sensor

    case @set_traffic_light
      when 'red'
        @arduino.digital_write RED_PIN, true
        @arduino.digital_write YELLOW_PIN, false
        @arduino.digital_write GREEN_PIN, false
      when 'yellow'
        @arduino.digital_write RED_PIN, false
        @arduino.digital_write YELLOW_PIN, true
        @arduino.digital_write GREEN_PIN, false
      when 'green'
        @arduino.digital_write RED_PIN, false
        @arduino.digital_write YELLOW_PIN, false
        @arduino.digital_write GREEN_PIN, true
    end
    sleep 1
  end


end
